const oauth2 = require("simple-oauth2");

module.exports = options => {
  const { db, oauth2 } = options;

  return (req, res, next) => {
    let code = req.query.code;
    let state = req.query.state;
    console.log(`code: ${code} state: ${state}`);

    const tokenConfig = {
      code,
      redirect_uri: "https://gitlab-integration.wstack.fi/api/oauth2response"
    };

    db
      .collection("authorizationRequests")
      .where("state", "==", state)
      .get()
      .then(results => {
        if (results.size === 1) {
          let authorizationRequest = null;
          results.forEach(doc => {
            authorizationRequest = doc.data();
          });
          console.info(
            "oauth2response.foundAuthorizationRequest",
            authorizationRequest
          );
          return authorizationRequest;
        } else if (results.empty) {
          throw new Error(
            "oauth2-response.failed.authorizationRequestNotFound"
          );
        } else {
          throw new Error(
            "oauth2-response.failed.multipleAuthorizationRequestsFound"
          );
        }
      })
      .then(authorizationRequest => {
        oauth2.authorizationCode.getToken(tokenConfig).then(accessToken => {
          console.info("oauth2response.gotToken", accessToken);
          return db
            .collection("authorizations")
            .doc(authorizationRequest.target)
            .set({
              accessToken
            });
        });
      })
      .then(() => {
        res.redirect("https://gitlab-integration.wstack.fi/");
      })
      .catch(err => {
        console.error(err);
        res.redirect(
          `https://gitlab-integration.wstack.fi/error?code=createAccessTokenFailed?${
            err.message
          }`
        );
      });
  };
};
