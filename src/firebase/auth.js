import { auth, google } from './firebase';

// Sign In
export const doSignInWithGoogle = () =>
  auth.signInWithPopup(google)


// Sign out
export const doSignOut = () =>
  auth.signOut();
