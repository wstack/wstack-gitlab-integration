'use strict'

const crypto = require('crypto')
const status = require('http-status')
const express = require('express')

module.exports = (options) => {
  const { db, oauth2 } = options
  const router = express.Router()

  router.post('/', (req, res, next) => {
    const authorizationRequest = {
      state: crypto.randomBytes(64).toString('hex'),
      target: 'gitlab.com'
    }
    db.collection('authorizationRequests').add(authorizationRequest)
      .then(ref => {
        authorizationRequest.redirectUrl =
          oauth2.authorizationCode.authorizeURL({
            redirect_uri: 'https://gitlab-integration.wstack.fi/api/oauth2response',
            scope: 'api',
            state: authorizationRequest.state
          })

        res
          .status(status.CREATED)
          .set('Location', ref.id)
          .json(authorizationRequest)
      })
      .catch(err => {
        console.error('authorizationRequests.create.failed', err)
        next(err)
      })
  })

  return router
}
