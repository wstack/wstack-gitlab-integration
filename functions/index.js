const functions = require('firebase-functions')
const admin = require('firebase-admin')
const express = require('express')
const cookieParser = require('cookie-parser')()
const cors = require('cors')

// Firebase init
admin.initializeApp({
  credential: admin.credential.applicationDefault()
})

// Oauth2 init
const gitLabCredentials = {
  client: {
    id: functions.config().gitlab.applicationid,
    secret: functions.config().gitlab.secret,
  },
  auth: {
    tokenHost: 'https://gitlab.com'
  }
}
const oauth2 = require('simple-oauth2').create(gitLabCredentials)

// CORS
const corsOptions = {
  origin: [ 'https://gitlab-integration.wstack.fi', 'http://localhost:8080' ]
}

const db = admin.firestore()
const authorizationRequestsController = require('./authorization-requests')({ db, oauth2 })
const oauth2ResponseController = require('./oauth2-response')({ db, oauth2 })

const app = express()
app.use(cookieParser)
app.get('/api/oauth2response', oauth2ResponseController)
app.use(
  '/api/authorizationrequests',
  cors(corsOptions),
  validateFirebaseIdToken,
  authorizationRequestsController
)

// https://gitlab-integration.wstack.fi/api
exports.api = functions.https.onRequest(app)

// Based on https://github.com/firebase/functions-samples/blob/master/authorized-https-endpoint/functions/index.js
// Express middleware that validates Firebase ID Tokens passed in the Authorization HTTP header.
// The Firebase ID token needs to be passed as a Bearer token in the Authorization HTTP header like this:
// `Authorization: Bearer <Firebase ID Token>`.
// when decoded successfully, the ID Token content will be added as `req.user`.
function validateFirebaseIdToken (req, res, next) {
  console.log('Check if request is authorized with Firebase ID token')

  if ((!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) &&
    !req.cookies.__session) {
    console.error('No Firebase ID token was passed as a Bearer token in the Authorization header.',
      'Make sure you authorize your request by providing the following HTTP header:',
      'Authorization: Bearer <Firebase ID Token>',
      'or by passing a "__session" cookie.')
    res.status(403).send('Unauthorized')
    return
  }

  let idToken
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
    console.log('Found "Authorization" header')
    // Read the ID Token from the Authorization header.
    idToken = req.headers.authorization.split('Bearer ')[1]
  } else {
    console.log('Found "__session" cookie')
    // Read the ID Token from cookie.
    idToken = req.cookies.__session
  }

  admin.auth().verifyIdToken(idToken).then((decodedIdToken) => {
    console.log('ID Token correctly decoded', decodedIdToken)
    req.user = decodedIdToken
    return next()
  }).catch((error) => {
    console.error('Error while verifying Firebase ID token:', error)
    res.status(403).send('Unauthorized')
  })
}
