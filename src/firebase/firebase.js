import * as firebase from "firebase";
import "firebase/firestore";

const config = {
  apiKey: "AIzaSyBzLmjrkNFxO34PJsTRUw1ytNne7skTj7w",
  authDomain: "wstack-gitlab-integration.firebaseapp.com",
  databaseURL: "https://wstack-gitlab-integration.firebaseio.com",
  projectId: "wstack-gitlab-integration",
  storageBucket: "wstack-gitlab-integration.appspot.com",
  messagingSenderId: "1031734063307"
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const auth = firebase.auth();
const google = new firebase.auth.GoogleAuthProvider();
const db = firebase.firestore();

export { auth, db, google };
