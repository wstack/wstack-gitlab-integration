import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Navigation from "./Navigation";
import LandingPage from "./Landing";
import SignInPage from "./SignIn";
import ConnectionsPage from "./Connections";

import * as routes from "../constants/routes";
import withAuthentication from "./withAuthentication";

const App = () => (
  <Router>
    <div>
      <Navigation />

      <hr />

      <Route exact path={routes.LANDING} component={() => <LandingPage />} />
      <Route exact path={routes.SIGN_IN} component={() => <SignInPage />} />
      <Route
        exact
        path={routes.CONNECTIONS}
        component={() => <ConnectionsPage />}
      />
    </div>
  </Router>
);

export default withAuthentication(App);
