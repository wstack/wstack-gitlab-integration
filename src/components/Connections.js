import React, { Component } from "react";

import withAuthorization from "./withAuthorization";
import ConnectWithGitLab from "./ConnectWithGitLab";
import { firebase } from "../firebase";

class ConnectionsPage extends Component {
  constructor(props) {
    super(props);
    this.state = { connections: [] };
  }

  componentDidMount() {
    firebase.db
      .collection("authorizations")
      .get()
      .then(snapshot => this.setState(() => ({ connections: snapshot.docs })));
  }

  render() {
    const { connections } = this.state;

    return (
      <div>
        <h1>Connections</h1>

        <ConnectionList connections={connections} />

        <div>
          <ConnectWithGitLab />
        </div>
      </div>
    );
  }
}

function ConnectionList(props) {
  const connections = props.connections;
  const connectionListItems = connections.map(connection => (
    <li key="{connection.id}">{connection.id}</li>
  ));
  return <ul>{connectionListItems}</ul>;
}

const authCondition = authUser => !!authUser;

export default withAuthorization(authCondition)(ConnectionsPage);
