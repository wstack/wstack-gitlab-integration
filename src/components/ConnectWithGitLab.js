import React, { Component } from 'react'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'

// TODO: Don't hard-code:
const wstackGitLabIntegrationApi = 'https://gitlab-integration.wstack.fi/api'

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
})

const INITIAL_STATE = {
  error: null,
}

class ConnectWithGitLabForm extends Component {
  constructor(props, { authUser }) {
    super(props);

    this.state = { ...INITIAL_STATE }
    this.authUser = authUser
  }

  static contextTypes = {
    authUser: PropTypes.object,
  }

  onSubmit = (event) => {
    this.authUser.getIdToken()
      .then(token => {
        return fetch(wstackGitLabIntegrationApi + '/authorizationrequests', {
          method: 'POST',
          headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
          })
        })
      })
      .then(res => {
        if (res.ok) {
          return res.json()
        } else {
          throw new Error(res.body)
        }
      })
      .then(authorizationRequest => {
        location = authorizationRequest.redirectUrl
      })
      .catch(error => {
        this.setState(byPropKey('error', JSON.stringify(error)))
      })

    event.preventDefault()
  }

  render() {
    const {
      error,
    } = this.state

    return (
      <form onSubmit={this.onSubmit}>
        <button type="submit">
          Connect with GitLab
        </button>

        { error && <p>{error.message}</p> }
      </form>
    )
  }
}

export default ConnectWithGitLabForm
